#include <Average.h>

//PID tuning parameters
#define KP 1 
#define KI 1
#define KD 1    

//motor speeds
#define leftMinSpeed 75
#define rightMinSpeed 75

#define leftMaxSpeed 150 
#define rightMaxSpeed 150 

// MOTOR pins
#define leftMotor1 8
#define leftMotor2 7
#define leftMotorPWM 9
#define rightMotor1 5
#define rightMotor2 4
#define rightMotorPWM 6

// IR sensor pins
// _ \ | / _
// 0 1 2 3 4
const int ir0 = A0;
const int ir1 = A1;
const int ir2 = A2;
const int ir3 = A3;
const int ir4 = A6;
int irInput[] = {A0,A1,A2,A3,A4,A6};

//creates array to store values from ir sensors
unsigned int sensorValues[5];

//encoder pins, using only one from each
#define leftEncA 2
#define rightEncA 3


//wheel dimensions used for encoder
const int wheelDiameter = 70.2;
const int wheelCircumference = PI * wheelDiameter;
const int pulsePerRev = 420;
const int cellSize = 175;
//clicks from center of one cell to the other
const int clicksPerCell = (pulsePerRev * cellSize) / wheelCircumference;

//encoder ticks
volatile int leftTicks = 0;
volatile int rightTicks = 0;

//position correction for PID 
int lastError = 0;
int lastProportional = 0;
int sumError = 0;

void setup()
{
	Serial.begin(9600);
	// Pin Assignments
  	// Motors
  	Serial.println("Assigning Motors");
  	pinMode(leftMotor1, OUTPUT);
  	pinMode(leftMotor2, OUTPUT);
  	pinMode(leftMotorPWM, OUTPUT);
  	pinMode(rightMotor1, OUTPUT);
  	pinMode(rightMotor2, OUTPUT);
  	pinMode(rightMotorPWM, OUTPUT);

  	// Encoders
  	Serial.println("Assigning Encoders");
  	pinMode(leftEncA, INPUT_PULLUP);
  	pinMode(rightEncA, INPUT_PULLUP);

  	//ir sensors to analog inputs, will return value of 0-1023
  	//uncessary for analogread but included for code readability 
  	Serial.println("Assigning IR sensors");
  	pinMode(ir0, INPUT);
  	pinMode(ir1, INPUT);
 	pinMode(ir2, INPUT);
 	pinMode(ir3, INPUT);
 	pinMode(ir4, INPUT);

 	// Encoder interrupts to detect changes
 	// may need to change mode type to rising or falling edge of input signal
 	Serial.println("Attaching interrupts to encoder pins");
  	attachInterrupt(digitalPinToInterrupt(leftEncA), countLeft, RISING);
  	attachInterrupt(digitalPinToInterrupt(rightEncA), countRight, RISING);

  	Serial.println("Setup Successful, bitches");
  	delay(250);

 }

 void loop()
 {
 		
 }

// function to move forward c amount of cells
 bool moveCell(int c){
 	while(c != 0){
 		while(countLeft != clicksPerCell && countRight != clicksPerCell)
 			moveForward();
 		c--;
 		resetTicks();
 	}
 	stopMoving();

 	return true;

 }

void moveForward() {
	readSensors();

 	//if position > 0 wall closer to right side of robot
 	//if position < 0 wall closer to left side of robot
  	unsigned int position = sensorValues[0] - sensorValues[4];

  	if (position == 0){
  		set_motors(leftMinSpeed,rightMinSpeed);
  	}
  	else{
  		//ideally error = 0
  		//using just one wall as the point of reference for correction
  		int error = sensorValues[0] - 32.5;
 	
 		int motorSpeed = KP * error + (1/KI) * sumError + KD*(error - lastError);

  		lastError = error;
  		sumError += error;

  		int leftMotorSpeed = leftMinSpeed - motorSpeed;
  		int rightMotorSpeed = rightMinSpeed + motorSpeed;

		set_motors(leftMotorSpeed, rightMotorSpeed);
  	}

}

void set_motors(int leftSpeed, int rightSpeed)
{
   if (leftSpeed > leftMaxSpeed )
      leftSpeed = leftMaxSpeed;
   if (rightSpeed > rightMaxSpeed ) 
      rightSpeed = rightMaxSpeed;
   if (leftSpeed < 0) 
      leftSpeed = 0; 
   if (rightSpeed < 0)
      rightSpeed = 0; 

    // move forward with appropriate speeds
    digitalWrite(rightMotor1, HIGH);
    digitalWrite(rightMotor2, LOW);
    analogWrite(rightMotorPWM, rightSpeed);
    digitalWrite(leftMotor1, HIGH);
    digitalWrite(leftMotor2, LOW);
    analogWrite(leftMotorPWM, leftSpeed);
}



void stopMoving(){
  digitalWrite(leftMotor1, LOW);
  digitalWrite(leftMotor2, LOW);
  analogWrite(leftMotorPWM, 0);
  digitalWrite(rightMotor1, LOW);
  digitalWrite(rightMotor2, LOW);
  analogWrite(rightMotorPWM, 0);  
  delay(50);
}

//read sensor value as standardized measurement
void readSensors(){
	int sampleSize = 4;


	for(int a = 0; a < 5; a++){
		Average <int> measurements (sampleSize);
		for(int i = 0; i< sampleSize; i++){
			measurements.push(analogRead(irInput[a]));
		}
		sensorValues[a] = measurements.mean();
	}


	/*
	sensorValues[0] = analogRead(ir0);
	sensorValues[1] = analogRead(ir1);
	sensorValues[2] = analogRead(ir2);
	sensorValues[3] = analogRead(ir3);
	sensorValues[4] = analogRead(ir4);
	*/

	for(int i = 0; i < 5;i++){
			sensorValues[i] = sensorValues[i]*(5/1023);
			//need proper conversion to standardized measurement
			//use below for reference, need to determine best fitting exponential decreasing equation
			//http://luckylarry.co.uk/arduino-projects/arduino-using-a-sharp-ir-sensor-for-distance-calculation/
			//65 and -1.10 taken from site
			sensorValues[i] = 65*pow(sensorValues[i],-1.10);
	}
}

 // ENCODER FUNCTIONS
void countLeft(){
  leftTicks++;
}
void countRight(){
  rightTicks++;
}
void resetTicks(){
	leftTicks = 0;
	rightTicks = 0;
}